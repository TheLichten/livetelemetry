﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiveTelemetryServer
{
    [Serializable]
    public class Vehicle
    {
        public string Login { get; internal set; }
        internal string Password { get; set; }
        public double DataTimeStamp { get; private set; }

        private Dictionary<string, string> StoredData;

        public Dictionary<string, TimeSpan> Drivers { get; private set; }
        private double DriverStart { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        private string ipAddress;

        public Vehicle()
        {
            Drivers = new Dictionary<string, TimeSpan>();
        }

        public Vehicle(string login, string password)
        {
            this.Login = login;
            this.Password = password;

            Drivers = new Dictionary<string, TimeSpan>();
        }


        public Dictionary<string, string> Data
        {
            get
            {
                return StoredData;
            }

            private set
            {

            }
        }

        internal void closeDriver(string ipAddress)
        {
            if (this.ipAddress != null || ipAddress != this.ipAddress)
            {
                //return; //Override at this moment
            }
            double timeStamp = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0)).TotalMilliseconds;

            if (DriverStart != -1)
            {
                TimeSpan lastStint = TimeSpan.FromMilliseconds(timeStamp - DriverStart);
                TimeSpan total = Drivers[StoredData["DriverName"]];

                total = total + lastStint;

                //Updating the total time
                Drivers.Remove(StoredData["DriverName"]);
                Drivers.Add(StoredData["DriverName"], total);

                //Adding the next driver
                DriverStart = -1;

                this.ipAddress = null; //unlocking
            }
        }

        internal void updateData(Dictionary<string, string> value, string ipAddress)
        {
            try
            {
                Dictionary<string, string> oldData = StoredData;
                //Patching up things
                if (Drivers == null)
                    Drivers = new Dictionary<string, TimeSpan>();

                if (oldData == null)
                {
                    oldData = new Dictionary<string, string>();

                    oldData.Add("SessionTypeName", "Traps Are Gay");

                    if (value.ContainsKey("DriverName"))
                        oldData.Add("DriverName", value["DriverName"]);
                    else
                        oldData.Add("DriverName", "");
                }

                //Now make the access check
                double timeStamp = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0)).TotalMilliseconds;

                if (this.ipAddress != null && ipAddress != this.ipAddress)
                {
                    bool fulfillsTimeout = (DataTimeStamp + 1000 * 15) < timeStamp;


                    //We are having wierd access
                    if (value.ContainsKey("DriverName"))
                    {
                        if (value["DriverName"] != oldData["DriverName"])
                        {
                            if (!fulfillsTimeout)
                            {
                                //Access locked
                                throw new AccessViolationException("Locking the other client out");
                            }
                            else
                            {
                                //this is a hand over
                            }
                        }
                        else
                        {
                            //everything is fine
                        }
                    }
                    else if (value.ContainsKey("SessionTypeName"))
                    {
                        if (value["SessionTypeName"] != oldData["SessionTypeName"])
                        {
                            if (!fulfillsTimeout)
                            {
                                //Access locked
                                throw new AccessViolationException("Locking the other client out");
                            }
                            else
                            {
                                //this is a hand over
                            }
                        }
                        else
                        {
                            //we assume everything is fine
                        }
                    }
                }


                //-------------------------------------------------------------------------------------------------------------------------
                //Regular Processing
                //-------------------------------------------------------------------------------------------------------------------------


                this.ipAddress = ipAddress;
                DataTimeStamp = timeStamp;

                //Adding Utc Time Stamp
                if (!value.ContainsKey("UtcDataStamp"))
                    value.Add("UtcDataStamp", ((long)DataTimeStamp) + "");

                //Endurance driver time protocoler
                if (value.ContainsKey("DriverName") && value.ContainsKey("SessionTypeName"))
                {
                    //Update driver database
                    if (value["SessionTypeName"] != oldData["SessionTypeName"])
                    {
                        DriverStart = DataTimeStamp;
                        Drivers.Clear();

                        Drivers.Add(value["DriverName"], new TimeSpan(0));
                    }
                    else if (DriverStart == -1) //Driver has swapped out of the car, now a new stint starts
                    {
                        //Adding the next driver
                        DriverStart = DataTimeStamp;

                        if (!Drivers.ContainsKey(value["DriverName"]))
                            Drivers.Add(value["DriverName"], new TimeSpan(0));
                    }
                    else if (value["DriverName"] != oldData["DriverName"] && oldData["DriverName"] != "") //Old system
                    {
                        TimeSpan lastStint = TimeSpan.FromMilliseconds(DataTimeStamp - DriverStart);
                        TimeSpan total = Drivers[oldData["DriverName"]];

                        total = total + lastStint;

                        //Updating the total time
                        Drivers.Remove(oldData["DriverName"]);
                        Drivers.Add(oldData["DriverName"], total);

                        //Adding the next driver
                        DriverStart = DataTimeStamp;

                        if (!Drivers.ContainsKey(value["DriverName"]))
                            Drivers.Add(value["DriverName"], new TimeSpan(0));
                    }
                    else if (!Drivers.ContainsKey(value["DriverName"])) //This is a wierd fallback, should never occure 
                    {
                        DriverStart = DataTimeStamp;
                        Drivers.Add(value["DriverName"], new TimeSpan(0));
                    }

                    //Adding Propertys
                    double driverStart = DriverStart;
                    if (driverStart == -1)
                        driverStart = DataTimeStamp;

                    TimeSpan thisStint = TimeSpan.FromMilliseconds(DataTimeStamp - DriverStart);
                    TimeSpan totalDriver = Drivers[value["DriverName"]];

                    totalDriver = totalDriver + thisStint;

                    value.Add("DriverStintTime", thisStint.Hours + ":" + thisStint.Minutes + ":" + thisStint.Seconds);
                    value.Add("DriverTotalTime", totalDriver.Hours + ":" + totalDriver.Minutes + ":" + totalDriver.Seconds);
                }
            }
            catch (AccessViolationException ex)
            {
                throw ex;
            }
            catch (Exception)
            {

            }


            //Writing the new data in
            StoredData = value;
        }


    }
}
