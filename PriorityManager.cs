﻿using GameReaderCommon;
using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiveTelemetry
{
    class PriorityManager
    {
        private const int MAXCOUNT = 5000; //~1.3 min
        private int counter;

        /// <summary>
        /// called one time per game data update
        /// </summary>
        /// <param name="pluginManager"></param>
        /// <param name="data"></param>
        public void PriorityCheck(PluginManager pluginManager, GameData data, bool gameHighPriority, bool simHubLowPriority)
        {
            //Boosting the priority of the game
            if (data.GameRunning && data.NewData != null && gameHighPriority)
            {
                //Saving time by checking irregularly
                if (counter < MAXCOUNT)
                {
                    counter++;
                    return;
                }

                Process pro = null;


                //Using get Process name
                if (pluginManager.GetProcesseNames() != null)
                {
                    var proNames = pluginManager.GetProcesseNames().GetEnumerator();

                    bool works = true;

                    works = proNames.MoveNext();

                    if (works)
                    {
                        string name = proNames.Current;

                        if (name != null)
                            pro = getProcess(name);
                    }
                }

                //Fall back
                if (pro != null)
                {
                    pro = getProcess(data.GameName);

                    if (pro == null && data.GamePath != null)
                    {
                        string execName = Path.GetFileName(data.GamePath);
                        pro = getProcess(execName);

                        if (pro == null)
                        {
                            pro = getProcess(Path.GetFileNameWithoutExtension(execName));
                        }
                    }
                }

                //Gameprocess is not there yet, but the game counts as running, so next tick we will return
                if (pro == null)
                {
                    counter++;

                    if ((counter - (MAXCOUNT / 4)) > MAXCOUNT)
                    {
                        //Simehub is acting strange, so we check back in MAXCOUNT x2
                        counter = 0 - MAXCOUNT;
                    }

                    return;
                }

                try
                {
                    if (pro.PriorityClass != ProcessPriorityClass.High)
                        pro.PriorityClass = ProcessPriorityClass.High;

                    if (!pro.PriorityBoostEnabled)
                        pro.PriorityBoostEnabled = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message + "\n" + e.StackTrace);
                }
                finally
                {
                    counter = 0;
                }
            }

            //Reducing SimHub Process Priority
            if (simHubLowPriority)
            {
                if (gameHighPriority && counter < (MAXCOUNT-1))
                {
                    //We wait till the next check
                    return;
                }
                else if (counter < MAXCOUNT) //High Game Priority is off
                {
                    counter++;
                    return;
                }


                try
                {
                    if (Process.GetCurrentProcess().PriorityClass != ProcessPriorityClass.BelowNormal)
                        Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.BelowNormal;

                }
                catch
                {

                }
                finally
                {
                    if (!gameHighPriority)
                        counter = 0;
                }
            }
            
            
        }


        private Process getProcess(string processName)
        {
            Process[] pros = Process.GetProcessesByName(processName);

            if (pros == null || pros.Length == 0)
                return null;

            return pros[0];
        }
    }
}
