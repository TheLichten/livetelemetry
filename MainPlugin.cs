﻿using GameReaderCommon;
using SimHub.Plugins;
using System;
using System.Windows.Forms;
using System.Windows.Controls;
using System.IO;
using System.Xml;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Threading;
using System.Xml.Serialization;

namespace LiveTelemetry
{

    [PluginName("LiveTelemetry")]
    public class LiveTelemetry : IPlugin, IDataPlugin, IWPFSettings
    {
        //Constants
        public const string SETTINGFILE = "PluginsData/LiveTelemetrySettings.xml";
        public const string KEYSFILE = "PluginsData/LiveTelemetryPropertys.setting";

        public readonly string[] DEFAULT_VALUE_KEYS = {
            "DataCorePlugin.GameData.NewData.TrackName",
            "DataCorePlugin.GameData.NewData.SessionTypeName",
            "DataCorePlugin.GameData.NewData.SessionTimeLeft",
            "DataCorePlugin.GameData.NewData.Position",
            "DataCorePlugin.GameData.NewData.Fuel",
            "DataCorePlugin.Computed.Fuel_RemainingLaps",
            "DataCorePlugin.Computed.Fuel_LastLapConsumption",
            "DataCorePlugin.Computed.Fuel_LitersPerLap",
            "DataCorePlugin.GameData.NewData.TyreWearFrontLeft",
            "DataCorePlugin.GameData.NewData.TyreWearFrontRight",
            "DataCorePlugin.GameData.NewData.TyreWearRearLeft",
            "DataCorePlugin.GameData.NewData.TyreWearRearRight",
            "DataCorePlugin.GameData.NewData.LastLapTyreWearFrontLeft",
            "DataCorePlugin.GameData.NewData.LastLapTyreWearFrontRight",
            "DataCorePlugin.GameData.NewData.LastLapTyreWearRearLeft",
            "DataCorePlugin.GameData.NewData.LastLapTyreWearRearRight",
            "DataCorePlugin.GameData.NewData.TyrePressureFrontLeft",
            "DataCorePlugin.GameData.NewData.TyrePressureFrontRight",
            "DataCorePlugin.GameData.NewData.TyrePressureRearLeft",
            "DataCorePlugin.GameData.NewData.TyrePressureRearRight",
            "DataCorePlugin.GameData.NewData.BrakeTemperatureFrontLeft",
            "DataCorePlugin.GameData.NewData.BrakeTemperatureFrontRight",
            "DataCorePlugin.GameData.NewData.BrakeTemperatureRearLeft",
            "DataCorePlugin.GameData.NewData.BrakeTemperatureRearRight",
            "DataCorePlugin.GameData.NewData.IsInPitLane",
            "DataCorePlugin.GameData.NewData.AirTemperature",
            "DataCorePlugin.GameData.NewData.RoadTemperature",
            "DataCorePlugin.GameData.NewData.OilTemperature",
            "DataCorePlugin.GameData.NewData.WaterTemperature",
            "DataCorePlugin.GameData.NewData.CurrentLap",
            "DataCorePlugin.GameData.NewData.LastLapTime",
            "DataCorePlugin.GameRawData.Data.mMaxPathWetness",
            "DataCorePlugin.GameRawData.Data.mMinPathWetness",
            "DataCorePlugin.GameRawData.Data.mRaining",
            "LiveTelemetry.LapFraction",
            "LiveTelemetry.DriverName",
            "LiveTelemetry.PosInClass",
            "LiveTelemetry.CarName"
        };

        public HttpClient Client { get; private set; }

        public string[] ValueKeys { get; private set; }

        //Variables
        public SettingsContainer Settings { get; private set; }

        public TelemetryMode Mode
        {
            get;
            internal set;
        }

        [Obsolete]
        public double tickCounter;

        public bool turnedOff;
        public Thread dataSendThread;

        public bool disabled;

        /// <summary>
        /// Instance of the current plugin manager
        /// </summary>
        public PluginManager PluginManager { get; set; }

        private LiveTelemetryReceiver Receiver { get; set; }

        private PriorityManager Priority { get; set; }

        
        public string ServerAddress
        {
            get { return Settings.ServerAddress; }
            internal set { Settings.ServerAddress = value; }
        }
        public string Login
        {
            get { return Settings.Login; }
            internal set { Settings.Login = value; }
        }
        public string Password
        {
            get { return Settings.Password; }
            internal set { Settings.Password = value; }
        }
        public int SendRate
        {
            get { return Settings.SendRate; }
            internal set { Settings.SendRate = value; }
        }

        /// <summary>
        /// Called after plugins startup
        /// </summary>
        /// <param name="pluginManager"></param>
        public void Init(PluginManager pluginManager)
        {
            Priority = new PriorityManager();
            //Client = new HttpClient();

            Receiver = new LiveTelemetryReceiver(this);

            FileStream stream = null;

            try
            {
                if (File.Exists(SETTINGFILE))
                {
                    stream = new FileStream(SETTINGFILE, FileMode.Open, FileAccess.Read, FileShare.Read);
                    XmlSerializer serializer = new XmlSerializer(new SettingsContainer().GetType());
                    

                    object obj = serializer.Deserialize(stream);

                    stream.Close();

                    if (obj is SettingsContainer)
                    {
                        Settings = (SettingsContainer)obj;

                        //Making sure to remove slashes at the end
                        while (ServerAddress.LastIndexOf('/') == (ServerAddress.Length - 1))
                        {
                            ServerAddress = ServerAddress.Substring(0, (ServerAddress.Length - 1));
                        }

                        if (ServerAddress.ToLower().Contains("http://"))
                            ServerAddress = ServerAddress.Substring("http://".Length);
                        if (ServerAddress.ToLower().Contains("https://"))
                            ServerAddress = ServerAddress.Substring("https://".Length);

                        Uri uri = new Uri("http://" + ServerAddress);

                    }
                    else
                    {
                        throw new FormatException("This is not my class");
                    }
                }
            }
            catch
            {
                Settings = new SettingsContainer();
                ServerAddress = null;
                Login = null;
                Password = null;
                SendRate = 10;

                if (stream != null)
                {
                    try
                    {
                        stream.Close();
                    }
                    catch
                    {
                    }
                }
            }

            setupHttpClient();

            pluginManager.AddProperty("ServerConnected", this.GetType(), true.GetType(), "If the Live Telemetry Plugin is connected");
            pluginManager.AddProperty("ClientMode", this.GetType(), TelemetryMode.None.GetType(), "Sending or Receiving Mode");
            pluginManager.AddProperty("ServerError", this.GetType(), "".GetType(), "If the Connection fails, here is the Error");

            pluginManager.AddProperty("UtcTimeStamp", this.GetType(), long.MaxValue.GetType(), "Utc Time Stamp to compare recieved packages to");

            pluginManager.AddProperty("LapFraction", this.GetType(), Double.MinValue.GetType(), "How far the car is around the lap");
            pluginManager.AddProperty("DriverName", this.GetType(), "".GetType(), "Name of the Driver");
            pluginManager.AddProperty("PosInClass", this.GetType(), "".GetType(), "Position in class");
            pluginManager.AddProperty("CarName", this.GetType(), "".GetType(), "Name and number of the car");


            stream = null;
            //VALUE_KEYS:
            if (File.Exists(KEYSFILE))
            {
                stream = new FileStream(KEYSFILE, FileMode.Open, FileAccess.Read, FileShare.Read);
                StreamReader keyReader = new StreamReader(stream);

                List<string> list = new List<string>();
                string line = "";
                while ((line = keyReader.ReadLine()) != null)
                {
                    line = line.Trim();

                    if (line != "")
                        list.Add(line);
                }

                ValueKeys = list.ToArray();

                keyReader.Close();
                stream.Close();
            }
            else
            {
                ValueKeys = DEFAULT_VALUE_KEYS;
                
                stream = new FileStream(KEYSFILE, FileMode.Create, FileAccess.Write, FileShare.None);
                StreamWriter keyWriter = new StreamWriter(stream);

                foreach (var item in DEFAULT_VALUE_KEYS)
                {
                    keyWriter.WriteLine(item);
                }

                keyWriter.Flush();
                keyWriter.Close();
                stream.Close();
            }

            Receiver.initReceiver(ValueKeys);

            Client.Timeout = new TimeSpan(0,0,2);

            if (Settings.disableAutoConnect)
                disabled = true;

            if (ServerAddress != null)
            {
                startDataThread();
            }

            pluginManager.AddAction("LiveTelemetryOnOff", this.GetType(), (a, b) =>
            {
                disabled = !disabled;
            });
        }

        internal void setupHttpClient()
        {
            var OldClient = this.Client;

            var credentials = new System.Net.NetworkCredential(Login, Password);
            var handler = new HttpClientHandler { Credentials = credentials };

            this.Client = new HttpClient(handler);

            if (OldClient != null)
            {
                try
                {
                    OldClient.Dispose();
                }
                catch (Exception)
                {
                }
            }
        }

        internal void startDataThread()
        {
            if (dataSendThread != null) //simple protection from it killing itself
                return;


            dataSendThread = new Thread(DataLoop);
            dataSendThread.Name = "Updater-Thread";
            dataSendThread.Priority = ThreadPriority.Normal;
            dataSendThread.Start();
        }

        public void SaveSettings()
        {
            XmlSerializer serializer = new XmlSerializer(new SettingsContainer().GetType());
            FileStream stream = new FileStream(SETTINGFILE, FileMode.Create, FileAccess.Write, FileShare.None);

            serializer.Serialize(stream, Settings);

            stream.Close();
        }

        /// <summary>
        /// called one time per game data update
        /// </summary>
        /// <param name="pluginManager"></param>
        /// <param name="data"></param>
        public void DataUpdate(PluginManager pluginManager, ref GameData data)
        {
            PluginManager = pluginManager;
            PluginManager.SetPropertyValue("ClientMode", this.GetType(), Mode);
            
            PluginManager.SetPropertyValue("UtcTimeStamp", this.GetType(), ((long)(DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0)).TotalMilliseconds));

            if (data.GameRunning && data.NewData != null && data.OldData != null)
            {

                Opponent player = data.NewData.OpponentAtPosition(1, true, false, true);

                pluginManager.SetPropertyValue("LapFraction", this.GetType(), player.TrackPositionPercent);
                pluginManager.SetPropertyValue("DriverName", this.GetType(), player.Name);
                pluginManager.SetPropertyValue("PosInClass", this.GetType(), player.PositionInClass);
                pluginManager.SetPropertyValue("CarName", this.GetType(), player.CarName + " #" + player.CarNumber);
            }

            if (ServerAddress == null || ServerAddress == "")
            {
                PluginManager.SetPropertyValue("ServerConnected", this.GetType(), false);
                PluginManager.SetPropertyValue("ServerError", this.GetType(), "Deactivated: Missing Sever Address");

                return;
            }


            //Mode updates
            if (Mode == TelemetryMode.Sending && (!data.GameRunning || data.GameInMenu))
            {
                Mode = TelemetryMode.Receiving;

                //Telling the Server that we are now ending
                Task<HttpResponseMessage> request = Client.GetAsync("http://" + ServerAddress + "/ending" + getURIHandel());
            }
            else if (Mode == TelemetryMode.Receiving && data.GameRunning && !data.GameInMenu)
            {
                Mode = TelemetryMode.Sending;
            }

            Priority.PriorityCheck(pluginManager, data, Settings.gameHighPriorityMode, Settings.simHubLowPriorityMode);
        }

        /// <summary>
        /// Used to send the Data
        /// </summary>
        private void DataLoop()
        {
            turnedOff = false;

            while (!turnedOff)
            {
                Thread.Sleep(1000 / SendRate);

                if (turnedOff)
                    return;

                if (!disabled)
                {
                    //Update
                    try
                    {
                        CancellationTokenSource cancelTokenSource = new CancellationTokenSource();

                        if (Mode == TelemetryMode.None)
                        {
                            Task<HttpResponseMessage> request = Client.GetAsync("http://" + ServerAddress + "/test" + getURIHandel());
                            request.ContinueWith(SendRespondsHandler, cancelTokenSource.Token);
                        }

                        if (Mode == TelemetryMode.Sending)
                        {
                            Dictionary<string, string> values = new Dictionary<string, string>();

                            foreach (string key in ValueKeys)
                            {
                                object obj = PluginManager.GetPropertyValue(key);
                                string val = "";

                                if (obj.GetType() == new TimeSpan().GetType())
                                {
                                    TimeSpan span = (TimeSpan)obj;

                                    if (key.Contains("SessionTimeLeft"))
                                    {
                                        val = span.Hours + ":" + span.Minutes + ":" + span.Seconds;
                                    }
                                    else
                                    {
                                        val = span.Minutes + ":" + span.Seconds + "." + span.Milliseconds;
                                    }
                                }
                                else
                                {
                                    val = obj.ToString();
                                }

                                string[] pieces = key.Split('.');
                                string keyTemp = pieces[pieces.Length - 1];

                                values.Add(keyTemp, val);
                            }

                            HttpContent content = new FormUrlEncodedContent(values);

                            Task<HttpResponseMessage> request = Client.PostAsync("http://" + ServerAddress + "/send" + getURIHandel(), content);
                            request.ContinueWith(SendRespondsHandler, cancelTokenSource.Token);
                        }

                        if (Mode == TelemetryMode.Receiving)
                        {
                            Task<HttpResponseMessage> request = Client.GetAsync("http://" + ServerAddress + "/receive" + getURIHandel());
                            request.ContinueWith(SendRespondsHandler, cancelTokenSource.Token);
                        }


                        cancelTokenSource.CancelAfter(2000);
                    }
                    catch
                    {

                    }
                }
                else
                {
                    //Setting this is to display to the user that the livetelemtry is disabled
                    PluginManager.SetPropertyValue("ServerConnected", this.GetType(), false);
                    PluginManager.SetPropertyValue("ServerError", this.GetType(), "Disabled");
                }
            }
        }

        /// <summary>
        /// Async Handler for requests
        /// </summary>
        /// <param name="task"></param>
        private void SendRespondsHandler(Task<HttpResponseMessage> task)
        {

            if (task.IsFaulted)
            {
                //Connection failed
                PluginManager.SetPropertyValue("ServerConnected", this.GetType(), false);

                Exception ex = task.Exception;

                if (ex.InnerException != null)
                    ex = ex.InnerException; //Removing the useless outer layer of a task exception

                if (ex.InnerException != null)
                    ex = ex.InnerException;

                if (ex != null)
                    PluginManager.SetPropertyValue("ServerError", this.GetType(), ex.Message);
                else
                    PluginManager.SetPropertyValue("ServerError", this.GetType(), "Unknown cancelation");

                Mode = TelemetryMode.None;
            }
            else if (task.IsCanceled)
            {
                PluginManager.SetPropertyValue("ServerConnected", this.GetType(), false);
                PluginManager.SetPropertyValue("ServerError", this.GetType(), "Unknown cancelation");
                Mode = TelemetryMode.None;
            }
            else if (task.IsCompleted)
            {
                //No exceptions in the thread

                HttpResponseMessage message = task.Result;

                if (!message.IsSuccessStatusCode)
                {
                    PluginManager.SetPropertyValue("ServerConnected", this.GetType(), false);
                    PluginManager.SetPropertyValue("ServerError", this.GetType(), message.StatusCode + ": " + message.ReasonPhrase);

                    Mode = TelemetryMode.None;
                    return;
                }
                else
                {
                    //Everything worked out

                    if (Mode == TelemetryMode.None)
                    {
                        //This means this was a ping
                        PluginManager.SetPropertyValue("ServerConnected", this.GetType(), true);
                        PluginManager.SetPropertyValue("ServerError", this.GetType(), null);

                        Mode = TelemetryMode.Sending;
                        return;
                    }
                    else if (Mode == TelemetryMode.Sending)
                    {
                        //This was a data send
                        PluginManager.SetPropertyValue("ServerConnected", this.GetType(), true);
                        PluginManager.SetPropertyValue("ServerError", this.GetType(), null);

                        Receiver.receiveDataAsync(message);
                    }
                    else if (Mode == TelemetryMode.Receiving)
                    {
                        //This was a data request
                        PluginManager.SetPropertyValue("ServerConnected", this.GetType(), true);
                        PluginManager.SetPropertyValue("ServerError", this.GetType(), null);

                        Receiver.receiveDataAsync(message);
                    }
                }
            }
            else
            {
                //This should be impossible to reach, but still
                return;
            }
        }

        private string getURIHandel()
        {
            return "?Login=" + Login + "&Password=" + Password;
        }

        /// <summary>
        /// Called at plugin manager stop, close/displose anything needed here !
        /// </summary>
        /// <param name="pluginManager"></param>
        public void End(PluginManager pluginManager)
        {
            turnedOff = true;
            Client.Dispose();
        }

        /// <summary>
        /// Return you winform settings control here, return null if no settings control
        /// 
        /// </summary>
        /// <param name="pluginManager"></param>
        /// <returns></returns>
        public System.Windows.Forms.Control GetSettingsControl(PluginManager pluginManager)
        {
            return null;
        }

        public System.Windows.Controls.Control GetWPFSettingsControl(PluginManager pluginManager)
        {
            return new SettingsControl(this);
        }

        public enum TelemetryMode
        {
            None, Sending, Receiving
        }

        [Serializable]
        public class SettingsContainer : SettingsControl.Profile
        {
            public bool gameHighPriorityMode;
            public bool simHubLowPriorityMode;

            public bool disableAutoConnect;
        }
    }
}