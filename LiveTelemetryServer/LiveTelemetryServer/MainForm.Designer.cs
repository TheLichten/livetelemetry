﻿namespace LiveTelemetryServer
{
    partial class MainForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gbSettings = new System.Windows.Forms.GroupBox();
            this.btnStartStop = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tbPort = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tbVehicleSearch = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblRequestRate = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblDiscordBot = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblLastSave = new System.Windows.Forms.ToolStripStatusLabel();
            this.timerUiUpdater = new System.Windows.Forms.Timer(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRefreshVehData = new System.Windows.Forms.Button();
            this.btnSaveVehData = new System.Windows.Forms.Button();
            this.btnRemoveVeh = new System.Windows.Forms.Button();
            this.btnEditVeh = new System.Windows.Forms.Button();
            this.btnCreateVeh = new System.Windows.Forms.Button();
            this.lbVehicleList = new System.Windows.Forms.ListBox();
            this.chkAutoStart = new System.Windows.Forms.CheckBox();
            this.gbSettings.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbSettings
            // 
            this.gbSettings.Controls.Add(this.chkAutoStart);
            this.gbSettings.Controls.Add(this.btnStartStop);
            this.gbSettings.Controls.Add(this.label2);
            this.gbSettings.Controls.Add(this.tbPort);
            this.gbSettings.Controls.Add(this.label1);
            this.gbSettings.Location = new System.Drawing.Point(12, 12);
            this.gbSettings.Name = "gbSettings";
            this.gbSettings.Size = new System.Drawing.Size(198, 201);
            this.gbSettings.TabIndex = 0;
            this.gbSettings.TabStop = false;
            this.gbSettings.Text = "Settings";
            // 
            // btnStartStop
            // 
            this.btnStartStop.Location = new System.Drawing.Point(71, 49);
            this.btnStartStop.Name = "btnStartStop";
            this.btnStartStop.Size = new System.Drawing.Size(100, 23);
            this.btnStartStop.TabIndex = 1;
            this.btnStartStop.Text = "Start";
            this.btnStartStop.UseVisualStyleBackColor = true;
            this.btnStartStop.Click += new System.EventHandler(this.btnStartStop_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Active:";
            // 
            // tbPort
            // 
            this.tbPort.Location = new System.Drawing.Point(71, 23);
            this.tbPort.Name = "tbPort";
            this.tbPort.Size = new System.Drawing.Size(100, 20);
            this.tbPort.TabIndex = 1;
            this.tbPort.Text = "5128";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Port:";
            // 
            // tbVehicleSearch
            // 
            this.tbVehicleSearch.Location = new System.Drawing.Point(6, 23);
            this.tbVehicleSearch.Name = "tbVehicleSearch";
            this.tbVehicleSearch.Size = new System.Drawing.Size(227, 20);
            this.tbVehicleSearch.TabIndex = 1;
            this.toolTip1.SetToolTip(this.tbVehicleSearch, "Search for a specific Account");
            this.tbVehicleSearch.TextChanged += new System.EventHandler(this.tbVehicleSearch_TextChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.lblRequestRate,
            this.lblDiscordBot,
            this.lblLastSave});
            this.statusStrip1.Location = new System.Drawing.Point(0, 419);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(784, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(81, 17);
            this.lblStatus.Text = "Status: Offline";
            // 
            // lblRequestRate
            // 
            this.lblRequestRate.Name = "lblRequestRate";
            this.lblRequestRate.Size = new System.Drawing.Size(104, 17);
            this.lblRequestRate.Text = "Requests/Min: 000";
            // 
            // lblDiscordBot
            // 
            this.lblDiscordBot.Name = "lblDiscordBot";
            this.lblDiscordBot.Size = new System.Drawing.Size(110, 17);
            this.lblDiscordBot.Text = "Discord Bot: Offline";
            // 
            // lblLastSave
            // 
            this.lblLastSave.Name = "lblLastSave";
            this.lblLastSave.Size = new System.Drawing.Size(145, 17);
            this.lblLastSave.Text = "Last Database Save: 00min";
            // 
            // timerUiUpdater
            // 
            this.timerUiUpdater.Interval = 1000;
            this.timerUiUpdater.Tick += new System.EventHandler(this.timerUiUpdater_Tick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnRefreshVehData);
            this.groupBox1.Controls.Add(this.btnSaveVehData);
            this.groupBox1.Controls.Add(this.btnRemoveVeh);
            this.groupBox1.Controls.Add(this.btnEditVeh);
            this.groupBox1.Controls.Add(this.btnCreateVeh);
            this.groupBox1.Controls.Add(this.tbVehicleSearch);
            this.groupBox1.Controls.Add(this.lbVehicleList);
            this.groupBox1.Location = new System.Drawing.Point(427, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(345, 300);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Account Database";
            // 
            // btnRefreshVehData
            // 
            this.btnRefreshVehData.Location = new System.Drawing.Point(239, 235);
            this.btnRefreshVehData.Name = "btnRefreshVehData";
            this.btnRefreshVehData.Size = new System.Drawing.Size(100, 23);
            this.btnRefreshVehData.TabIndex = 7;
            this.btnRefreshVehData.Text = "Refresh";
            this.btnRefreshVehData.UseVisualStyleBackColor = true;
            this.btnRefreshVehData.Click += new System.EventHandler(this.btnRefreshVehData_Click);
            // 
            // btnSaveVehData
            // 
            this.btnSaveVehData.Location = new System.Drawing.Point(239, 264);
            this.btnSaveVehData.Name = "btnSaveVehData";
            this.btnSaveVehData.Size = new System.Drawing.Size(100, 23);
            this.btnSaveVehData.TabIndex = 6;
            this.btnSaveVehData.Text = "Save Database";
            this.btnSaveVehData.UseVisualStyleBackColor = true;
            this.btnSaveVehData.Click += new System.EventHandler(this.btnSaveVehData_Click);
            // 
            // btnRemoveVeh
            // 
            this.btnRemoveVeh.Location = new System.Drawing.Point(239, 107);
            this.btnRemoveVeh.Name = "btnRemoveVeh";
            this.btnRemoveVeh.Size = new System.Drawing.Size(100, 23);
            this.btnRemoveVeh.TabIndex = 5;
            this.btnRemoveVeh.Text = "Remove";
            this.btnRemoveVeh.UseVisualStyleBackColor = true;
            this.btnRemoveVeh.Click += new System.EventHandler(this.btnRemoveVeh_Click);
            // 
            // btnEditVeh
            // 
            this.btnEditVeh.Location = new System.Drawing.Point(239, 78);
            this.btnEditVeh.Name = "btnEditVeh";
            this.btnEditVeh.Size = new System.Drawing.Size(100, 23);
            this.btnEditVeh.TabIndex = 4;
            this.btnEditVeh.Text = "Edit";
            this.btnEditVeh.UseVisualStyleBackColor = true;
            this.btnEditVeh.Click += new System.EventHandler(this.btnEditVeh_Click);
            // 
            // btnCreateVeh
            // 
            this.btnCreateVeh.Location = new System.Drawing.Point(239, 49);
            this.btnCreateVeh.Name = "btnCreateVeh";
            this.btnCreateVeh.Size = new System.Drawing.Size(100, 23);
            this.btnCreateVeh.TabIndex = 3;
            this.btnCreateVeh.Text = "New";
            this.btnCreateVeh.UseVisualStyleBackColor = true;
            this.btnCreateVeh.Click += new System.EventHandler(this.btnCreateVeh_Click);
            // 
            // lbVehicleList
            // 
            this.lbVehicleList.FormattingEnabled = true;
            this.lbVehicleList.Location = new System.Drawing.Point(6, 49);
            this.lbVehicleList.Name = "lbVehicleList";
            this.lbVehicleList.Size = new System.Drawing.Size(227, 238);
            this.lbVehicleList.TabIndex = 0;
            // 
            // chkAutoStart
            // 
            this.chkAutoStart.AutoSize = true;
            this.chkAutoStart.Location = new System.Drawing.Point(71, 82);
            this.chkAutoStart.Name = "chkAutoStart";
            this.chkAutoStart.Size = new System.Drawing.Size(73, 17);
            this.chkAutoStart.TabIndex = 3;
            this.chkAutoStart.Text = "Auto Start";
            this.chkAutoStart.UseVisualStyleBackColor = true;
            this.chkAutoStart.CheckedChanged += new System.EventHandler(this.chkAutoStart_CheckedChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 441);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.gbSettings);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Live Telemetry Server";
            this.gbSettings.ResumeLayout(false);
            this.gbSettings.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbSettings;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbPort;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnStartStop;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.ToolStripStatusLabel lblRequestRate;
        private System.Windows.Forms.ToolStripStatusLabel lblDiscordBot;
        private System.Windows.Forms.Timer timerUiUpdater;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStripStatusLabel lblLastSave;
        private System.Windows.Forms.TextBox tbVehicleSearch;
        private System.Windows.Forms.ListBox lbVehicleList;
        private System.Windows.Forms.Button btnSaveVehData;
        private System.Windows.Forms.Button btnRemoveVeh;
        private System.Windows.Forms.Button btnEditVeh;
        private System.Windows.Forms.Button btnCreateVeh;
        private System.Windows.Forms.Button btnRefreshVehData;
        private System.Windows.Forms.CheckBox chkAutoStart;
    }
}

