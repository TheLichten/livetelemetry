﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LiveTelemetryServer
{

    public class Database
    {
        public Dictionary<string, Vehicle> Values { get; private set; }

        private string filePath;
        public DateTime lastSave { get; private set; }

        public bool StopAutoSave { get; set; }

        public Database(string filePath)
        {
            this.filePath = filePath;

            if (File.Exists(filePath))
            {
                //TODO: Deserialization
                try
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    Stream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                    Values = (Dictionary<string, Vehicle>)formatter.Deserialize(stream);
                    stream.Close();
                }
                catch
                {
                    Values = new Dictionary<string, Vehicle>();
                }
            }
            else
            {
                //Creating a new Database
                Values = new Dictionary<string, Vehicle>();

                //Making Shure the folder exists
                string folder = Path.GetDirectoryName(filePath);

                if (folder != null && folder != "")
                {
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                }

                //Saving:
                Save();

                //Testing only:
                Values.Add("Test", new Vehicle("Test", "12345"));
            }

            Thread thread = new Thread(AutoSave);
            thread.Name = "AutoSave";
            thread.Priority = ThreadPriority.BelowNormal;
            thread.Start();

        }

        public Vehicle GetVehicle(string login, string password)
        {
            if (Values.ContainsKey(login))
            {
                Vehicle veh = Values[login];

                if (veh.Password.Equals(password))
                {
                    return veh;
                }
            }

            return null;
        }

        public void Save()
        {
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                Stream stream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None);
                formatter.Serialize(stream, Values);

                stream.Close();

                //Creating Backup:
                DateTime now = DateTime.UtcNow;
                lastSave = now;

                string date = now.Year + ".";

                if (now.Month < 10)
                    date = date + "0" + now.Month + ".";
                else
                    date = date + now.Month + ".";

                if (now.Day < 10)
                    date = date + "0" + now.Day;
                else
                    date = date + now.Day;

                date = date + "-";

                if (now.Hour < 10)
                    date = date + "0" + now.Hour + ".";
                else
                    date = date + now.Hour + ".";

                if (now.Minute < 10)
                    date = date + "0" + now.Minute + ".";
                else
                    date = date + now.Minute + ".";

                if (now.Second < 10)
                    date = date + "0" + now.Second;
                else
                    date = date + now.Second;

                File.Copy(filePath, filePath + "." + date + ".bak", true);
            }
            catch
            {

            }
        }

        private void AutoSave()
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            while (!StopAutoSave)
            {
                for (int i = 0; i < 3600*6 && !StopAutoSave; i++)
                {
                    Thread.Sleep(1000);
                }

                Save();
            }
        }
    }
}
